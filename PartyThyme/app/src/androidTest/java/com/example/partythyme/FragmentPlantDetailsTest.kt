package com.example.partythyme

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.partythyme.data.models.Plant
import com.example.partythyme.fragments.plant_details.PlantDetailsFragment
import com.nhaarman.mockitokotlin2.mock
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

private const val IMG = "ic_launcher_background"


@RunWith(AndroidJUnit4::class)
class FragmentPlantDetailsTest {

    private fun createDummyPlant() : Plant {
        return Plant(
            1L,
            "name",
            "nickname",
            IMG
        )
    }

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    @Test
    fun check_fragment_is_visible(){
        //given
        val fragment = PlantDetailsFragment()
        fragment.presenter = mock()

        //when
        activityRule.activity.showFragment(fragment)

        //then
        onView(withId(R.id.fpd_parent_layout)).check(matches(isDisplayed()))

    }

}