package com.example.partythyme


import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.partythyme.EspressoUtils.withIndex
import com.example.partythyme.data.models.Plant
import com.example.partythyme.fragments.main_list.MainListFragment
import com.example.partythyme.fragments.main_list.MainListFragmentContract
import com.nhaarman.mockitokotlin2.clearInvocations
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

private const val IMG = "ic_launcher_background"

@RunWith(AndroidJUnit4::class)
class FragmentMainListTest  {

    private fun createDummyPlants(size: Int) : List<Plant>{
        return List(size) { index ->
            Plant(
                index.toLong(),
                "name$index",
                "nickname$index",
                IMG
//            emptyList()
            )
        }
    }

    @get:Rule
    val activityRule = ActivityTestRule(TestActivity::class.java)

    @Test
    fun check_main_list_fragment_visible(){
        //given
        val fragment =
            MainListFragment()
        fragment.presenter = mock()

        //when

        activityRule.runOnUiThread {
            activityRule.activity.showFragment(fragment)

//        activityRule.activity.showFragment(fragment)

            //then
            onView(withId(R.id.fml_parent_layout)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun on_view_created_notifies_presenter() {
        // given
        val fragment =
            MainListFragment()

        activityRule.activity.showFragment(fragment)

        onView(withId(R.id.fml_parent_layout)).check(matches(isDisplayed()))

        fragment.testing = true
        val presenter = mock<MainListFragmentContract.Presenter>()
        fragment.presenter = presenter

        // when
        activityRule.runOnUiThread {
            clearInvocations(presenter)
            fragment.onViewCreated(fragment.view!!, null)

            // then
            verify(presenter).onViewCreated()
        }
    }

    @Test
    fun check_RV_displays_correct_data(){
        // given
        val fragment =
            MainListFragment()
        fragment.presenter = mock()
        activityRule.activity.showFragment(fragment)

        onView(withId(R.id.test_root_layout)).check(matches(isDisplayed()))

        val plants = createDummyPlants(3)

        // when
        activityRule.runOnUiThread { fragment.showPlants(plants) }
        Thread.sleep(300)

        // then

        for ((index, plant) in plants.withIndex()) {
            onView(withIndex(R.id.fml_name, index)).check(matches(withText(plant.name)))
            onView(withIndex(R.id.fml_nickname, index)).check(matches(withText(plant.nickname)))
//            onView(withIndex(R.id.fml_image, index)).check(matches(withResourceName(plant.mainImage)))

        }
    }



    @Test
    fun on_plant_clicked_notifies_presenter() {
        // given
        val fragment =
            MainListFragment()
        fragment.testing = true
        val presenter = mock<MainListFragmentContract.Presenter>()
        fragment.presenter = presenter
        activityRule.activity.showFragment(fragment)

        onView(withId(R.id.test_root_layout)).check(matches(isDisplayed()))

        val plants = createDummyPlants(3)

        activityRule.runOnUiThread { fragment.showPlants(plants) }
        Thread.sleep(500)

        val indexOfClickedPlant = 1

        // when
        onView(withIndex(R.id.fml_ll_container, indexOfClickedPlant)).perform(click())

        // then
        verify(presenter).onPlantClicked(plants[indexOfClickedPlant])
    }





}