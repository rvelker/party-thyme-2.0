package com.example.partythyme.fragments.note_fragment

import com.example.partythyme.DefaultDispatcherProvider
import com.example.partythyme.DispatcherProvider
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.models.PlantNote
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val TAG = "tag"


class NoteFragmentPresenter (private val db : PlantInterface, private val dispatchers : DispatcherProvider = DefaultDispatcherProvider()) : NoteFragmentContract.Presenter {

    private lateinit var view: NoteFragmentContract.ViewInterface

    override fun setView(viewInterface: NoteFragmentContract.ViewInterface) {
        view = viewInterface
    }

    override fun onViewCreated(noteID: Long) {
        CoroutineScope(dispatchers.io()).launch {
            val note = getNote(noteID)

            //remove later
//            val tempComments = mutableListOf("string1", "string2", "string3", "string4")
//            val tempImgs = mutableListOf("ic_launcher_background", "ic_launcher_background", "ic_launcher_background", "ic_launcher_background")
//            showNoteContents(tempComments, tempImgs)
//
            showNoteContents(note.comments, note.images)
        }
    }

    private suspend fun showNoteContents(comments : MutableList<String>?, images : MutableList<String>?) {
        withContext(dispatchers.main()){view.showNoteContents(comments, images)}
    }

    override fun onCommentClicked(comment: String) {
        view.showClickedComment(comment)
    }

    override fun onImageClicked(img: String) {
        view.showClickedImage(img)
    }

    override fun onAddCommentClicked() {
        view.showAddCommentPopup()
    }

    override fun onAddImageClicked() {
        view.showAddImageClickedToast()
    }

    override fun onAddCommentConfirmClicked(comment: String, noteID: Long) {
        if (comment != "") {
            CoroutineScope(dispatchers.io()).launch {
                val note = getNote(noteID)
                note.comments!!.add(0, comment)
                updateNote(note)
                showNewComment(comment)
            }
        }
    }

    private suspend fun getNote(id : Long) : PlantNote {
        return  withContext(dispatchers.io()) { db.getNoteByNoteId(id)}
    }
    private suspend fun updateNote(note : PlantNote) {
         db.insertOrUpdateNote(note)
    }
    private suspend fun showNewComment(comment: String) {
        withContext(dispatchers.main()) { view.notifyAdapterNewCommentAdded(comment) }
    }

    override fun onDestroy() {
        TODO("Not yet implemented")
    }


}
