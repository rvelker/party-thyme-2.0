package com.example.partythyme.data

import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.models.PlantNote

interface PlantInterface {

    suspend fun getPlants() : List<Plant>?
    suspend fun getPlantById(id : Long) : Plant
    suspend fun getNotesByPlantId(plantId : Long) : List<PlantNote>?
    suspend fun addNewNote(plantNote: PlantNote)
    suspend fun getNoteByDate(date : Long) : PlantNote
    suspend fun getNoteByNoteId(noteID: Long): PlantNote
    suspend fun insertOrUpdateNote(note : PlantNote) : Long

}