package com.example.partythyme.fragments.note_fragment.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.partythyme.R
import kotlinx.android.synthetic.main.fragment_note_comments_list_item.view.*

private const val TAG = "tag"

class NoteFragmentCommentsAdapter(private val clickListener: CommentClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = mutableListOf<String>()
    private var newItem = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_note_comments_list_item, parent, false)
        return CommentViewHolder(view, clickListener)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder){
            is CommentViewHolder -> holder.bind(items[position], position)
        }
    }
    fun submitList(comments : List<String>) {
        for (comment in comments){
            items.add(comment)
        }
        notifyDataSetChanged()
    }

    fun addNewItem(comment : String) {
        newItem = true
        items.add(0, comment)
        notifyItemInserted(0)
    }

    class CommentViewHolder (view : View, clickListener: CommentClickListener) : RecyclerView.ViewHolder(view) {
        private lateinit var comment : String
        private var button : Button = view.fncli_button

        init {
            button.setOnClickListener { clickListener.onCommentClicked(comment) }
        }

        fun bind(comment : String, pos : Int){
            val letter = getLetterFromNumber(pos)
            this.comment = comment
            button.text = letter
            Log.d(TAG+"comm adapt", comment)

        }


        private fun getLetterFromNumber(i: Int): String {
            if (i < 0) {
                return "-" + getLetterFromNumber(-i - 1)
            }
            val quot = i / 26
            val rem = i % 26
            val letter = ('A'.toInt() + rem).toChar()
            return if (quot == 0) {
                Log.d(TAG+"lettergenerator", "" + letter )
                "" + letter
            } else {
                Log.d(TAG+"lettergenerator2", "" + letter )
                getLetterFromNumber(quot - 1) + letter
            }
        }

    }



}

interface CommentClickListener{
    fun onCommentClicked(comment : String)
}