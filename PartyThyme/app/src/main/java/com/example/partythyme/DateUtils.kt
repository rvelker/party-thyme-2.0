package com.example.partythyme

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class DateUtils {

    object TimeShow {
        @JvmStatic
        fun main(args: Array<String>) {
            try {
                val format = SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss")
                val past: Date = format.parse("2016.02.05 AD at 23:59:30")
                val now = Date()
                val seconds: Long =
                    TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime())
                val minutes: Long =
                    TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime())
                val hours: Long =
                    TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime())
                val days: Long = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime())

                when {
                    seconds < 60 -> {
                        println("$seconds seconds ago")
                    }
                    minutes < 60 -> {
                        println("$minutes minutes ago")
                    }
                    hours < 24 -> {
                        println("$hours hours ago")
                    }
                    else -> {
                        println("$days days ago")
                    }
                }
            } catch (j: Exception) {
                j.printStackTrace()
            }
        }
    }

    fun getConstantDate() : Long {
        return 15428715123
    }
}