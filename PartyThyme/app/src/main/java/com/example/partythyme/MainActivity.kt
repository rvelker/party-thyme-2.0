package com.example.partythyme

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.partythyme.fragments.main_list.MainListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try { this.supportActionBar!!.hide() } catch (e: NullPointerException) { }

        initMainListFragment()
    }

    private fun initMainListFragment() {
        val mainListFragment = MainListFragment.newInstance()
        supportFragmentManager
            .beginTransaction()
            .add(R.id.root_layout, mainListFragment, "main_list_fragment")
            .commit()
    }
}
