package com.example.partythyme.data

import androidx.room.*
import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.models.PlantNote

@Dao
interface PlantDao {

    @Query("SELECT * FROM plants")
    suspend fun getPlants() : List<Plant>

    @Query("SELECT * FROM plants WHERE plantId = :id")
    suspend fun getPlantById(id : Long) : Plant

    @Query("SELECT * FROM plant_notes WHERE date = :date")
    suspend fun getNoteByDate(date : Long) : PlantNote

    @Query("SELECT * FROM plant_notes")
    suspend fun getAllNotes() : List<PlantNote>

    @Query("SELECT * FROM plant_notes WHERE parentPlantId = :id ORDER BY date DESC")
    suspend fun getNotesByPlantId(id : Long) : List<PlantNote>

    @Query("SELECT * FROM plant_notes WHERE noteId = :id")
    suspend fun getNoteByNoteId(id : Long) : PlantNote

    @Delete
    suspend fun deletePlant(plant : Plant)

    @Delete
    suspend fun deleteNote(note : PlantNote)

    @Delete
    suspend fun deleteAllPlants(plants : List<Plant>)

    @Delete
    suspend fun deleteAllNotes(notes : List<PlantNote>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdatePlant(plant : Plant) : Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOrUpdateNote(note : PlantNote) : Long

}