package com.example.partythyme.fragments.plant_details

import com.example.partythyme.base_mvp.BasePresenter
import com.example.partythyme.data.models.PlantNote
import com.example.partythyme.popups.PopupListener

interface PlantDetailsContract  {
    interface ViewInterface : PopupListener {
        fun showNotes(notes : List<PlantNote>?)
        fun showNoteFragment(note : PlantNote)
        fun showAddNotePopup()
        fun getPopupText() : String
        fun showNewNoteTitleIsTooLongMsg()
        fun showNewNoteTitleIsEmptyMsg()
        fun dismissPopup()
        fun notifyAdapterNewNoteCreated(note: PlantNote)

    }

    interface Presenter : BasePresenter, OnNoteClickedListener {
        fun setView(viewInterface: ViewInterface)
        fun onViewCreated(plantID : Long)
        fun onAddNoteClicked()
        fun onAddNoteConfirmClicked(plantId: Long, text: String)

    }
}
