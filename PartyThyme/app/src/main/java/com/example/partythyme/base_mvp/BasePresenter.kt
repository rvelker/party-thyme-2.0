package com.example.partythyme.base_mvp

interface BasePresenter {

    fun onDestroy()
}