package com.example.partythyme.fragments.note_fragment.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.partythyme.GlideTemplate
import com.example.partythyme.R
import kotlinx.android.synthetic.main.fragment_note_image_list_item.view.*

private const val TAG = "tag"


class NoteFragmentImageAdapter (private val clickListener : NoteImageClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val glide = GlideTemplate()
    private val items = mutableListOf<String>()
    private var newItem = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_note_image_list_item, parent, false)
        return NoteImageViewHolder(
            view,
            clickListener
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun submitList(images : List<String>) {
        for (img in images) {
            items.add(img)
        }
        notifyDataSetChanged()
    }

    fun addNewItem(img : String) {
        newItem = true
        items.add(0, img)
        notifyItemInserted(0)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is NoteImageViewHolder -> holder.bind(items[position], glide)
        }
    }

    private class NoteImageViewHolder(view : View, clickListener: NoteImageClickListener) : RecyclerView.ViewHolder(view) {

        private lateinit var img : String
        val imageView : ImageView = view.fnili_image

        init {
            imageView.setOnClickListener {
                clickListener.onImageClicked(img)
            }
        }

        fun bind(img : String, glide: GlideTemplate) {
            this.img = img
            glide.bind(imageView.context, imageView, img)
            Log.d(TAG+"img adapt", img)


        }
    }

}

interface NoteImageClickListener {
    fun onImageClicked(img : String)
}