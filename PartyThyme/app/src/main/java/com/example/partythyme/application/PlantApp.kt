package com.example.partythyme.application

import android.app.Application
import com.example.partythyme.dagger.AppComponent
import com.example.partythyme.dagger.AppModule
import com.example.partythyme.dagger.DaggerAppComponent

class PlantApp : Application() {

    lateinit var component : AppComponent

    override fun onCreate() {
        super.onCreate()
        component = initDagger(this)
    }

    private fun initDagger(app : PlantApp) : AppComponent =
        DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()

}