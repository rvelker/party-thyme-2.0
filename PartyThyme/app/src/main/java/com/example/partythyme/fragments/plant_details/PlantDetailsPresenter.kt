package com.example.partythyme.fragments.plant_details

import com.example.partythyme.DefaultDispatcherProvider
import com.example.partythyme.DispatcherProvider
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.models.PlantNote
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class PlantDetailsPresenter (private val db : PlantInterface, private val dispatchers: DispatcherProvider = DefaultDispatcherProvider()) : PlantDetailsContract.Presenter {

    private lateinit var view : PlantDetailsContract.ViewInterface
    // todo this could be extracted to a different class: abstract class AbsPresenter<T>() { abstract var view: T  } where T is the view interface

    override fun setView(viewInterface: PlantDetailsContract.ViewInterface) {
        view = viewInterface
    }

    override fun onViewCreated(plantID : Long) {

        CoroutineScope(dispatchers.io()).launch  {
            val notes = getAllNotes(plantID)
            showAllNotes(notes)
        }
    }

    override fun onNoteClicked(note: PlantNote) {
        view.showNoteFragment(note)
    }

    override fun onAddNoteClicked() {
        view.showAddNotePopup()
    }

    override fun onAddNoteConfirmClicked(plantId: Long, text: String) {

        if (checkNoteTitleEmpty(text)) {
            view.showNewNoteTitleIsEmptyMsg()
            return
        }
        if (checkNoteTitleTooLong(text)){
            view.showNewNoteTitleIsTooLongMsg()
            return
        }

        view.dismissPopup()

        CoroutineScope(dispatchers.io()).launch {

            val date = getDate()
            val newNote = PlantNote(plantId, 0L, text, date, mutableListOf(), mutableListOf())

            addNoteToDb(newNote)
            val note = getNewNote(date)
            note?.let {showNewNote(it)}
        }
    }

    private suspend fun getAllNotes(plantId: Long) : List<PlantNote>?{
        return withContext(dispatchers.io()) {
            db.getNotesByPlantId(plantId)
        }
    }

    private suspend fun showAllNotes(notes : List<PlantNote>?) {
        return withContext(dispatchers.main()) {
            view.showNotes(notes)
        }
    }

    private suspend fun getNewNote(date : Long) : PlantNote? {
       return withContext(dispatchers.io()){
           db.getNoteByDate(date)
       }
    }

    private suspend fun showNewNote(note : PlantNote){
        withContext(dispatchers.main()) {
            view.notifyAdapterNewNoteCreated(note)
        }
    }

    private suspend fun addNoteToDb(note : PlantNote) {
        withContext(dispatchers.io()) {
            db.addNewNote(note)
        }
    }



    private fun checkNoteTitleTooLong(title: String): Boolean {
        return title.trim().length > 100
    }
    private fun checkNoteTitleEmpty(title: String): Boolean {
        return title.trim() == ""
    }
    private fun getDate() : Long {
        return Date().time
    }
    override fun onDestroy() {
        TODO("Not yet implemented")
    }


}

