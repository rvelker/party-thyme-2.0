package com.example.partythyme.data.models

import androidx.room.*
import com.example.partythyme.data.Converters


@Entity(
    tableName = "plant_notes"
)
@TypeConverters(Converters::class)
data class PlantNote (

    var parentPlantId : Long,

    @PrimaryKey (autoGenerate = true)
    var noteId : Long,

    var title : String,

    var date: Long,

    var comments : MutableList<String>? = mutableListOf(),

    var images : MutableList<String>? = mutableListOf()
)
