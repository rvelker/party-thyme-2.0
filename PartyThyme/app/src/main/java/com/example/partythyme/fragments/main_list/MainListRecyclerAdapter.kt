package com.example.partythyme.fragments.main_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.partythyme.GlideTemplate
import com.example.partythyme.R
import com.example.partythyme.data.models.Plant
import kotlinx.android.synthetic.main.fragment_main_list_item.view.*

class MainListRecyclerAdapter(private val onPlantClickedListener: OnPlantClickedListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = mutableListOf<Plant>()
    private val glide = GlideTemplate()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_main_list_item, parent, false)

        return PlantViewHolder(
            view,
            onPlantClickedListener
        )
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PlantViewHolder -> {
                holder.bind(items[position], glide)
            }
        }
    }

    fun submitList(plants: List<Plant>) {
        for (plant in plants) {
            items.add(plant)
        }
        notifyDataSetChanged()
    }

    fun addNewItem(plant : Plant) {
        items.add(0, plant)
        notifyItemInserted(0)
    }

    class PlantViewHolder(view: View, onPlantClickedListener: OnPlantClickedListener) : RecyclerView.ViewHolder(view) {


        init {
            itemView.setOnClickListener { onPlantClickedListener.onPlantClicked(plant) }
        }

        private lateinit var plant: Plant

        private val imageView = view.fml_image
        private val nameView = view.fml_name
        private val nicknameView = view.fml_nickname


        fun bind(plant: Plant, glide: GlideTemplate) {
            this.plant = plant

            nameView.text = plant.name
            nicknameView.text = plant.nickname
            glide.bind(imageView.context, imageView, plant.mainImage)

        }

    }

}

interface OnPlantClickedListener {
    fun onPlantClicked(plant: Plant)

}
