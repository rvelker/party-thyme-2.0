package com.example.partythyme.data

import android.content.Context
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.models.PlantNote
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.lang.reflect.Type
import java.util.*

@Database(
    entities = [Plant::class, PlantNote::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class RoomPlantDatabase : RoomDatabase() {


    abstract fun plantDao(): PlantDao

    companion object {

        @Volatile
        private var INSTANCE: RoomPlantDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): RoomPlantDatabase {
            val tempInstance =
                INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext, RoomPlantDatabase::class.java, "plant_database"
                )
                    .addCallback(
                        PlantDatabaseCallback(
                            scope
                        )
                    )
                    .build()
                INSTANCE = instance
                return instance
            }
        }

    }

    private class PlantDatabaseCallback(private val scope: CoroutineScope) :
        RoomDatabase.Callback() {
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database -> scope.launch {
                populateDatabase(database.plantDao())
//                createDummyNotes(database.plantDao())
            } }
        }

        suspend fun populateDatabase(dao: PlantDao) {
            val currentPlants = dao.getPlants()

            currentPlants.let{
                dao.deleteAllPlants(currentPlants)
            }

            val plants = List(5) { index ->
                Plant(
                    0,
                    "name$index",
                    "nickname$index",
                    "ic_launcher_background"
                )
            }

            for (plant in plants) {
                dao.insertOrUpdatePlant(plant)
            }

        }
        suspend fun createDummyNotes(dao : PlantDao) {
            val existingNotes = dao.getAllNotes()

            dao.deleteAllNotes(existingNotes)

//            val plants = dao.getPlants()
//
//            val date = Date().time
//
//            for (plant in plants) {
//
//                var x = 0
//
//                val notes = List(5) {index ->
//                    PlantNote(
//                        plant.plantId,
//                        (x + index).toLong(),
//                        "title $index",
//                        date,
//                        "",
//                        ""
//                    )
//                }
//
//                for (note in notes) {
//                    dao.insertNote(note)
//                }
//                x += 5
//
//
//            }
        }

    }
}

class Converters {
    @TypeConverter
    fun stringToList(value: String?): MutableList<String> {
        val listType: Type = object : TypeToken<ArrayList<String?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun listToString(list: MutableList<String?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}

