package com.example.partythyme.dagger

import android.app.Application
import com.example.partythyme.data.PlantDao
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.PlantRepo
import com.example.partythyme.data.RoomPlantDatabase
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.GlobalScope
import javax.inject.Singleton

@Module
class DBModule {

    @Provides
    @Singleton
    fun providePlantInterface(dao : PlantDao): PlantInterface = PlantRepo(dao)

    @Singleton
    @Provides
    fun providePlantDao(app: Application) : PlantDao {
        return RoomPlantDatabase.getDatabase(app, GlobalScope).plantDao()
    }

}