package com.example.partythyme.dagger

import com.example.partythyme.data.PlantInterface
import com.example.partythyme.fragments.plant_details.PlantDetailsContract
import com.example.partythyme.fragments.plant_details.PlantDetailsPresenter
import dagger.Module
import dagger.Provides

@Module
class PlantDetailsPresenterModule {

    @Provides
    fun providePlantDetailsPresenter(plantInterface: PlantInterface) : PlantDetailsContract.Presenter {
        return PlantDetailsPresenter(plantInterface)
    }

}