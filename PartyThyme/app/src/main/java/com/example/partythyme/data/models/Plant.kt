package com.example.partythyme.data.models

import androidx.room.*

@Entity(
    tableName = "plants"
)
data class Plant (

    @PrimaryKey (autoGenerate = true)
    var plantId : Long,

    var name : String,

    var nickname : String?,

    var mainImage : String

)