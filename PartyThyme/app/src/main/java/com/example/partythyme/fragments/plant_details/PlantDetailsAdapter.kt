package com.example.partythyme.fragments.plant_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.partythyme.R
import com.example.partythyme.data.models.PlantNote
import kotlinx.android.synthetic.main.fragment_plant_details_list_item.view.*

class PlantDetailsAdapter (private val clickListener: OnNoteClickedListener): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items = mutableListOf<PlantNote>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_plant_details_list_item, parent, false)

        return PlantDetailsViewHolder(
            view,
            clickListener
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is PlantDetailsViewHolder -> {
                holder.bind(items[position])
            }
        }

    }

    fun submitList(list : List<PlantNote>) {
        for(item in list){
            items.add(item)
        }
        notifyDataSetChanged()

    }
    fun insertNewNote(note : PlantNote){
        items.add(0, note)
        notifyItemInserted(0)
    }

    private class PlantDetailsViewHolder(view : View, clickListener : OnNoteClickedListener) : RecyclerView.ViewHolder(view) {

        private lateinit var plantNote : PlantNote

        private var titleView = view.fpdli_title
        private var dateView = view.fpdli_date

        init{
            itemView.setOnClickListener { clickListener.onNoteClicked(plantNote) }
        }

        fun bind (plantNote : PlantNote) {
            this.plantNote = plantNote

            titleView.text = plantNote.title
//            dateView.text = plantNote.date.toString()
            dateView.text = plantNote.noteId.toString()
        }
    }
}

interface OnNoteClickedListener {
    fun onNoteClicked(note: PlantNote)
}