package com.example.partythyme.views.popup

import android.content.Context
import android.util.AttributeSet
import android.widget.Button
import com.example.partythyme.R

class CustomButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatButton(context, attrs, defStyleAttr) {

    init {
        setBackgroundResource(R.drawable.custom_button_bg)
    }

}