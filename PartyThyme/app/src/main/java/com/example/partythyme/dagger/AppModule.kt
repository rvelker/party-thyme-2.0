package com.example.partythyme.dagger

import android.app.Application
import android.content.Context
import com.example.partythyme.data.PlantDao
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.PlantRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule (private val app : Application) {
    @Provides
    @Singleton
    fun provideContext() : Context = app

    @Provides
    @Singleton
    fun provideApp() : Application = app

}