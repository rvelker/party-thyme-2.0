package com.example.partythyme.fragments.note_fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.partythyme.GlideTemplate
import com.example.partythyme.R
import com.example.partythyme.application.PlantApp
import com.example.partythyme.fragments.note_fragment.adapters.NoteFragmentCommentsAdapter
import com.example.partythyme.fragments.note_fragment.adapters.NoteFragmentImageAdapter
import com.example.partythyme.popups.PopupAddNewNote
import kotlinx.android.synthetic.main.fragment_note.*
import kotlinx.android.synthetic.main.popup.*
import javax.inject.Inject

private const val TAG = "tag"

class NoteFragment : Fragment(), NoteFragmentContract.ViewInterface {

    companion object {
        fun newInstance(noteID : Long) : NoteFragment {
            val args = Bundle()
            args.putLong("noteID", noteID)
            val fragment = NoteFragment()
            fragment.arguments = args
            return fragment
        }
    }
    @Inject lateinit var presenter : NoteFragmentContract.Presenter
    private lateinit var imageAdapter: NoteFragmentImageAdapter
    private lateinit var commentAdapter : NoteFragmentCommentsAdapter
    private lateinit var glide : GlideTemplate
    private lateinit var addCommentPopup: PopupAddNewNote

    private var noteId = 0L
    private var testing = false


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_note, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noteId = requireArguments().get("noteID") as Long


        injectDependencies()
        setupRVs()
        setupAddButtons()

        presenter.onViewCreated(noteId)


    }

    private fun setupAddButtons() {
        fn_add_comment_btn.setOnClickListener {
            presenter.onAddCommentClicked()
        }
        fn_add_image_btn.setOnClickListener {
            presenter.onAddImageClicked()
        }
    }
    private fun setupRVs() {
//        glide = GlideTemplate()
        imageAdapter = NoteFragmentImageAdapter(presenter)
        commentAdapter = NoteFragmentCommentsAdapter(presenter)

        fn_images_RV.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL ,false)
        fn_comments_RV.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL ,false)

        fn_images_RV.adapter = imageAdapter
        fn_comments_RV.adapter = commentAdapter
    }
    private fun injectDependencies() {
        if (!testing) {
            (requireActivity().application as PlantApp).component.inject(this)
            presenter.setView(this)
        }
    }

    override fun showNoteContents(comments: List<String>?, images: List<String>?) {
        images?.let { imageAdapter.submitList(it)
            Log.d(TAG+"img fr", images.size.toString())

        } ?: run { showNoImages() }

        comments?.let { commentAdapter.submitList(it)
            Log.d(TAG+"comm fr", comments.size.toString())

        } ?: run { showNoComments() }

        fn_main_image.setBackgroundResource(R.drawable.ic_launcher_background)
    }



    private fun showNoComments() {}
    private fun showNoImages() {}


    override fun showClickedComment(comment: String) {
        current_comment_text_view.text = comment
    }
    override fun showClickedImage(img: String) {
        glide.bind(this.requireContext(), fn_main_image, img)
    }



    override fun notifyAdapterNewImageAdded(img: String) {
        imageAdapter.addNewItem(img)
    }
    override fun notifyAdapterNewCommentAdded(comment: String) {
        commentAdapter.addNewItem(comment)
    }



    override fun showAddCommentPopup() {
        addCommentPopup = PopupAddNewNote("Add comment")
        addCommentPopup.show(childFragmentManager, null)
        addCommentPopup.popupListener = this
        return
    }
    override fun onPopupConfirmClicked() {
        val text = getPopupText()
        presenter.onAddCommentConfirmClicked(text, noteId)
    }
    override fun onPopupCancelClicked() {}
    private fun getPopupText(): String {
        return addCommentPopup.popup_edit_text.toString()
    }
    override fun dismissPopup() {
        addCommentPopup.dismiss()
    }

    override fun showNewCommentTextIsEmptyMsg() {
        TODO("Not yet implemented")
    }
    override fun showAddImageClickedToast() {
        Toast.makeText(requireContext(),"Add image clicked",Toast.LENGTH_SHORT).show()
    }
}
