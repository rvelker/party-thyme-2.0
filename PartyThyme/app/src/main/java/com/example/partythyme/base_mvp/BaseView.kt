package com.example.partythyme.base_mvp

interface BaseView<T> {
    fun setPresenter(presenter : T)
}