package com.example.partythyme.dagger

import com.example.partythyme.data.*
import com.example.partythyme.fragments.main_list.MainListFragmentContract
import com.example.partythyme.fragments.main_list.MainListFragmentPresenter
import com.example.partythyme.fragments.note_fragment.NoteFragmentContract
import com.example.partythyme.fragments.note_fragment.NoteFragmentPresenter
import com.example.partythyme.fragments.plant_details.PlantDetailsContract
import com.example.partythyme.fragments.plant_details.PlantDetailsPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainListPresenterModule{

    @Provides
    fun provideMainListPresenter(plantInterface: PlantInterface) : MainListFragmentContract.Presenter {
       return MainListFragmentPresenter(plantInterface)
    }






}