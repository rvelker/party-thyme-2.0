package com.example.partythyme.dagger

import com.example.partythyme.MainActivity
import com.example.partythyme.fragments.main_list.MainListFragment
import com.example.partythyme.fragments.note_fragment.NoteFragment
import com.example.partythyme.fragments.plant_details.PlantDetailsFragment
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, MainListPresenterModule::class, NotePresenterModule::class, PlantDetailsPresenterModule::class, DBModule::class])

interface AppComponent {

    fun inject(target : MainListFragment)
    fun inject(target : MainActivity)
    fun inject(target : PlantDetailsFragment)
    fun inject(target : NoteFragment)

}