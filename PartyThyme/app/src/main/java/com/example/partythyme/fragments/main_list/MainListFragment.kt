package com.example.partythyme.fragments.main_list

import android.Manifest
import android.app.AlertDialog
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.partythyme.BuildConfig
import com.example.partythyme.ImageUtils
import com.example.partythyme.R
import com.example.partythyme.application.PlantApp
import com.example.partythyme.data.models.Plant
import com.example.partythyme.fragments.plant_details.PlantDetailsFragment
import kotlinx.android.synthetic.main.fragment_main_list.*
import javax.inject.Inject

const val CAMERA_REQUEST_CODE = 101

class MainListFragment : Fragment(),
    MainListFragmentContract.ViewInterface {

    companion object{

        fun newInstance(): MainListFragment {
            return MainListFragment()
        }
    }

    @Inject lateinit var presenter: MainListFragmentContract.Presenter
    private lateinit var adapter: MainListRecyclerAdapter

    var testing = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main_list, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        injectDependencies()
        initRV()
        setupAddPlantButton()

        presenter.onViewCreated()
    }

    private fun setupAddPlantButton() {
        fml_add_plant_button.setOnClickListener {
            checkPermission(Manifest.permission.CAMERA, "Camera", CAMERA_REQUEST_CODE)
        }
    }

    private fun checkPermission(permission : String, name : String, requestCode : Int) : Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            when {
                ContextCompat.checkSelfPermission(this.requireContext(), permission) == PackageManager.PERMISSION_GRANTED -> {
                    Toast.makeText(this.requireContext(), "$name permission granted", Toast.LENGTH_SHORT).show()
                }
                shouldShowRequestPermissionRationale(permission) -> showDialog(permission, name, requestCode)

                else -> ActivityCompat.requestPermissions(this.requireActivity(), arrayOf(permission), requestCode)
            }
        }
        return ContextCompat.checkSelfPermission(this.requireContext(), permission) == PackageManager.PERMISSION_GRANTED
    }

    private fun showDialog(permission: String, name: String, requestCode: Int) {
        val builder = AlertDialog.Builder(this.requireContext())

        builder.apply {
            setMessage("Permission to access your $name is required to use this app")
            setTitle("Permission required")
            setPositiveButton("OK"){dialog, which ->
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(permission), requestCode)
            }
        }
        val dialog = builder.create()
        dialog.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        fun innerCheck(name : String) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this.requireContext(), "$name permission denied", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this.requireContext(), "$name permission granted", Toast.LENGTH_SHORT).show()
            }
        }
        when (requestCode) {
            CAMERA_REQUEST_CODE -> innerCheck("Camera")
        }
    }


    private fun injectDependencies() {
        if (!testing) {
            (requireActivity().application as PlantApp).component.inject(this)
            presenter.setView(this)
        }
    }

    private fun initRV() {
        adapter = MainListRecyclerAdapter(presenter)
        fml_recycler_view.layoutManager = LinearLayoutManager(this.context)
        fml_recycler_view.adapter = adapter
    }


    override fun showPlants(plants: List<Plant>?) {
        plants?.let {
            adapter.submitList(plants)
        } ?: run {
            showNoPlantsFound()
        }
    }

    override fun showNoPlantsFound() {
        TODO("Not yet implemented")
    }


    override fun showPlantDetailsFragment(plantId: Long, plantImage : String) {

        val fragment = PlantDetailsFragment.newInstance(plantId, plantImage)
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.fml_details_layout, fragment)
            .addToBackStack(null)
            .commit()

    }

}