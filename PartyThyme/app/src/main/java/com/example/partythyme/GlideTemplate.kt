package com.example.partythyme

import android.content.Context
import android.graphics.Bitmap
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class GlideTemplate {

    private val requestOptions = RequestOptions()
        .placeholder(R.drawable.ic_launcher_background)
        .error(R.drawable.ic_launcher_background)

    fun bind(context: Context, view : ImageView, image : String){

        Glide.with(context)
            .applyDefaultRequestOptions(requestOptions)
            .load(image)
            .into(view)
    }

}