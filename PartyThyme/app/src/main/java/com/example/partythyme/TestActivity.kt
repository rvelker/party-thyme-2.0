package com.example.partythyme

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment

class TestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

    }

    fun showFragment(fragment : Fragment) {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.test_root_layout, fragment, fragment.javaClass.simpleName)
            .commit()
    }

}
