package com.example.partythyme.popups

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.example.partythyme.R
import kotlinx.android.synthetic.main.layout_two_buttons.*
import kotlinx.android.synthetic.main.popup.*

class PopupAddNewNote(popupTitle : String) : DialogFragment() {

    var popupListener : PopupListener? = null
    var title = popupTitle

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.popup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setup()
    }

    private fun setup() {
        dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        popup_title.text = title
        popup_edit_text.bringToFront()

        ltb_b_confirm?.setOnClickListener {
            popupListener?.onPopupConfirmClicked()
        }

        ltb_b_cancel?.setOnClickListener {
            popupListener?.onPopupCancelClicked()
            this.dismiss()
        }
    }


}

interface PopupListener {
    fun onPopupConfirmClicked()

    fun onPopupCancelClicked()

}