package com.example.partythyme

import com.squareup.okhttp.Dispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.Dispatchers.Unconfined

interface DispatcherProvider {
    fun main() : CoroutineDispatcher = Main
    fun default() : CoroutineDispatcher = Default
    fun io() : CoroutineDispatcher = IO
    fun unconfined() : CoroutineDispatcher = Unconfined

}

class DefaultDispatcherProvider : DispatcherProvider