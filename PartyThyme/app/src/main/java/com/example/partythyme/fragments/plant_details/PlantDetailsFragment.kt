package com.example.partythyme.fragments.plant_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.partythyme.GlideTemplate
import com.example.partythyme.R
import com.example.partythyme.application.PlantApp
import com.example.partythyme.data.models.PlantNote
import com.example.partythyme.fragments.note_fragment.NoteFragment
import com.example.partythyme.popups.PopupAddNewNote
import kotlinx.android.synthetic.main.fragment_plant_details.*
import kotlinx.android.synthetic.main.popup.*
import javax.inject.Inject

private const val TAG = "tag"

class PlantDetailsFragment : Fragment(), PlantDetailsContract.ViewInterface {

    companion object {
        fun newInstance(id : Long, image : String) : PlantDetailsFragment{

            val args = Bundle()
            args.putLong("plantID", id)
            args.putString("plant_image", image)

            val fragment = PlantDetailsFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject lateinit var presenter : PlantDetailsContract.Presenter
    private lateinit var adapter: PlantDetailsAdapter
    private var plantId: Long = 0L
    private lateinit var addNotePopup : PopupAddNewNote
    private var testing = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_plant_details, container, false )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        plantId = (requireArguments().get("plantID") as Long)

        injectDependencies()
        loadImage()
        setupRV()
        setupFAB()
        presenter.onViewCreated(plantId)
    }

    //region SETUP
    private fun loadImage() {
        val image = (requireArguments().get("plant_image")) as String
        GlideTemplate().bind(requireContext(),fpd_main_image, image)
    }

    private fun setupFAB() {
        fpd_add_note_button.setOnClickListener {
            presenter.onAddNoteClicked()
        }
    }

    private fun setupRV() {
        adapter = PlantDetailsAdapter(presenter)
        fpd_recycler_view.layoutManager = LinearLayoutManager(this.context)
        fpd_recycler_view.adapter = adapter
    }

    private fun injectDependencies() {
        if (!testing) {
            (requireActivity().application as PlantApp).component.inject(this)
            presenter.setView(this)
        }
    }
    //endregion

    override fun showNotes(notes: List<PlantNote>?) {
        notes?.let {
            adapter.submitList(notes)
        } ?: run {
            showNoNotes()
        }
    }

    private fun showNoNotes() {
    }

    override fun showNoteFragment(note: PlantNote) {
        val fragment = NoteFragment.newInstance(note.noteId)
        parentFragmentManager
            .beginTransaction()
            .replace(R.id.fpd_parent_layout, fragment)
            .addToBackStack(null)
            .commit()
    }

    override fun showAddNotePopup() {
        addNotePopup = PopupAddNewNote("Add note:")
        addNotePopup.show(childFragmentManager, null)
        addNotePopup.popupListener = this
        return
    }

    override fun showNewNoteTitleIsTooLongMsg() {
        Toast.makeText(requireContext(),"Note title is too long",Toast.LENGTH_SHORT).show()
    }

    override fun showNewNoteTitleIsEmptyMsg() {
        Toast.makeText(requireContext(),"Note needs a title",Toast.LENGTH_SHORT).show()
    }

    override fun dismissPopup() {
        addNotePopup.dismiss()
    }

    override fun notifyAdapterNewNoteCreated(note : PlantNote) {
        adapter.insertNewNote(note)
        fpd_recycler_view.scrollToPosition(0)
    }

    override fun onPopupConfirmClicked() {
        val text = getPopupText()
        presenter.onAddNoteConfirmClicked(plantId, text)
    }

    override fun getPopupText(): String {
        return addNotePopup.popup_edit_text.text.toString()
    }

    override fun onPopupCancelClicked() {}


}