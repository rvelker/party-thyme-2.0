package com.example.partythyme.fragments.main_list

import com.example.partythyme.base_mvp.BasePresenter
import com.example.partythyme.data.models.Plant

interface MainListFragmentContract {

    interface ViewInterface {
        fun showPlants(plants: List<Plant>?)
        fun showNoPlantsFound()
        fun showPlantDetailsFragment(plantId : Long, plantImage : String)
    }

    interface Presenter : BasePresenter, OnPlantClickedListener {
        fun setView(viewInterface: ViewInterface)
        fun onViewCreated()
        fun onAddPlantClicked()
    }
}