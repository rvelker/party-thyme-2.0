package com.example.partythyme.fragments.note_fragment


import com.example.partythyme.base_mvp.BasePresenter
import com.example.partythyme.fragments.note_fragment.adapters.CommentClickListener
import com.example.partythyme.fragments.note_fragment.adapters.NoteImageClickListener
import com.example.partythyme.fragments.plant_details.OnNoteClickedListener
import com.example.partythyme.popups.PopupListener

interface NoteFragmentContract {

    interface ViewInterface : PopupListener   {
        fun showNoteContents(comments : List<String>?, images : List<String>?)
        fun notifyAdapterNewImageAdded(img : String)
        fun notifyAdapterNewCommentAdded(comment: String)
        fun showAddCommentPopup()
        fun dismissPopup()
        fun showNewCommentTextIsEmptyMsg()
        fun showClickedComment(comment: String)
        fun showClickedImage(img : String)
        fun showAddImageClickedToast()
    }

    interface Presenter : BasePresenter, CommentClickListener, NoteImageClickListener {
        fun setView(viewInterface: ViewInterface)
        fun onViewCreated(noteID : Long)
        fun onAddImageClicked()
        fun onAddCommentClicked()
        fun onAddCommentConfirmClicked(comment : String, noteID: Long)
    }

}


