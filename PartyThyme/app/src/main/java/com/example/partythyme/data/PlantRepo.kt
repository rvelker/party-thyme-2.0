package com.example.partythyme.data

import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.models.PlantNote

class PlantRepo(private val dao : PlantDao) : PlantInterface {

    override suspend fun getPlants(): List<Plant>? {
        return dao.getPlants()
    }

    override suspend fun getPlantById(id: Long): Plant {
        return dao.getPlantById(id)
    }

    override suspend fun getNotesByPlantId(plantId: Long): List<PlantNote>? {
        return dao.getNotesByPlantId(plantId)
    }

    override suspend fun addNewNote(plantNote: PlantNote) {
        dao.insertOrUpdateNote(plantNote)
    }

    override suspend fun getNoteByDate(date: Long): PlantNote {
        return dao.getNoteByDate(date)
    }

    override suspend fun getNoteByNoteId(noteID: Long): PlantNote {
        return dao.getNoteByNoteId(noteID)
    }
    override suspend fun insertOrUpdateNote(note: PlantNote): Long {
        return dao.insertOrUpdateNote(note)
    }


}