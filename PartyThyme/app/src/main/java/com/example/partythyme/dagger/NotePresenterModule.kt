package com.example.partythyme.dagger

import com.example.partythyme.data.PlantInterface
import com.example.partythyme.fragments.note_fragment.NoteFragmentContract
import com.example.partythyme.fragments.note_fragment.NoteFragmentPresenter
import dagger.Module
import dagger.Provides

@Module
class NotePresenterModule {

    @Provides
    fun provideNotePresenter(plantInterface: PlantInterface) : NoteFragmentContract.Presenter {
        return NoteFragmentPresenter(plantInterface)
    }

}