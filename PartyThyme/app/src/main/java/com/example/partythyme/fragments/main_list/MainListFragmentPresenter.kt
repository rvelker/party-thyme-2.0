package com.example.partythyme.fragments.main_list


import com.example.partythyme.DefaultDispatcherProvider
import com.example.partythyme.DispatcherProvider
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.models.Plant
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainListFragmentPresenter(private val db: PlantInterface, private val dispatchers: DispatcherProvider = DefaultDispatcherProvider()) : MainListFragmentContract.Presenter {

    private lateinit var view : MainListFragmentContract.ViewInterface

    override fun setView(viewInterface: MainListFragmentContract.ViewInterface) {
        this.view = viewInterface
    }

    override fun onViewCreated() {

        CoroutineScope(dispatchers.io()).launch {
            val plants = getPlants()
            showPlants(plants)
        }
    }

    override fun onAddPlantClicked() {

    }

    private suspend fun getPlants() : List<Plant>? {
        return withContext(dispatchers.io()) {
            db.getPlants()
        }
    }
    private suspend fun showPlants(plants : List<Plant>?) {
        withContext(dispatchers.main()){
            view.showPlants(plants)
        }
    }

    override fun onPlantClicked(plant: Plant) {
        view.showPlantDetailsFragment(plant.plantId, plant.mainImage)
    }

    override fun onDestroy() {
    }



}