package com.example.partythyme.fragments.note_fragment

import com.example.partythyme.CoroutineTestRule
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.models.PlantNote
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class FragmentNotePresenterTests {

    @get:Rule
    var ctr = CoroutineTestRule()

    private lateinit var presenter : NoteFragmentPresenter
    @Mock private lateinit var view : NoteFragmentContract.ViewInterface
    @Mock private lateinit var db : PlantInterface

    @Before
    fun initMocks(){
        MockitoAnnotations.initMocks(this)
        presenter = NoteFragmentPresenter(db, ctr.testDispatcherProvider)
        presenter.setView(view)
    }


    @Test
    fun on_view_created_updates_comments_and_pictures() = ctr.testDispatcher.runBlockingTest {
        //given
        val note = createDummyNote_s(1)[0]
        whenever(db.getNoteByNoteId(note.noteId)).thenReturn(note)

        //when
        presenter.onViewCreated(note.noteId)

        //then
        verify(view).showNoteContents(note.comments, note.images)
    }
    @Test
    fun on_valid_comment_created_note_is_updated_in_db() = ctr.testDispatcher.runBlockingTest {
        //given
        val note = createDummyNote_s(1)[0]
        whenever(db.getNoteByNoteId(note.noteId)).thenReturn(note)
        val comment = "stringggggg"

        //when
        presenter.onAddCommentConfirmClicked(comment, note.noteId)

        //then
//        verify(db).insertOrUpdateNote(note)
        argumentCaptor<PlantNote>().apply {
            Mockito.verify(db).insertOrUpdateNote(capture())
            Assert.assertEquals(note.comments!![0], firstValue.comments!![0])
            Assert.assertEquals(note.noteId, firstValue.noteId)
        }
    }

    @Test
    fun on_valid_comment_created_shows_comment_in_view() = ctr.testDispatcher.runBlockingTest {
        //given
        val note = createDummyNote_s(1)[0]
        val comment = "stringggggg"
        whenever(db.getNoteByNoteId(note.noteId)).thenReturn(note)
        //when
        presenter.onAddCommentConfirmClicked(comment, note.noteId)
        //then
        verify(view).notifyAdapterNewCommentAdded(comment)
    }

    @Test
    fun on_add_comment_clicked_shows_popup(){
        //given

        //when
        presenter.onAddCommentClicked()

        //then
        verify(view).showAddCommentPopup()
    }

//    @Test
//    fun on_picture_taken_picture_added_to_database() = ctr.testDispatcher.runBlockingTest {
//        //given
//        val note = createDummyNote_s(1)[0]
//
//        //when
//
//        //then
//        verify(db).addNewNote(note)
//    }


//    @Test
//    fun on_add_picture_ok_clicked_picture_shown_in_view_RV(){
//
//    }

    @Test
    fun on_comment_clicked_sets_text_in_view(){
        //given
        val note = createDummyNote_s(1)[0]
        //when
        presenter.onCommentClicked(note.comments!![0])
        //then
        verify(view).showClickedComment(note.comments!![0])

    }

    @Test
    fun on_image_clicked_image_is_shown(){
        //given
        val note = createDummyNote_s(1)[0]
        //when
        presenter.onImageClicked(note.images!![0])
        //then
        verify(view).showClickedImage(note.images!![0])

    }

}

private fun createDummyNote_s(listSize : Int) : List<PlantNote> {
    val date = 15428715123

    return List(listSize) {index ->
        PlantNote(index.toLong(), index.toLong(), "title", date, mutableListOf("comments"), mutableListOf("ic_launcher_background"))
    }

}