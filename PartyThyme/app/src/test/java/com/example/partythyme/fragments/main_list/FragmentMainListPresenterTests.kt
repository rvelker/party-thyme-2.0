package com.example.partythyme.fragments.main_list

import com.example.partythyme.CoroutineTestRule
import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.PlantInterface
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test
import org.junit.Before
import org.junit.Rule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class FragmentMainListPresenterTests {

    @get:Rule
    var coroutinesTestRule = CoroutineTestRule()

    private lateinit var presenter : MainListFragmentPresenter
    @Mock lateinit var view : MainListFragmentContract.ViewInterface
    @Mock lateinit var db : PlantInterface
//    @Mock lateinit var dispatcher : DefaultDispatcherProvider


    @Before
    fun initMocks(){
      MockitoAnnotations.initMocks(this)
        presenter = MainListFragmentPresenter(db, coroutinesTestRule.testDispatcherProvider)
        presenter.setView(view)
    }

    @Test
    fun on_view_created_updates_plants_on_view() = coroutinesTestRule.testDispatcher.runBlockingTest {
        //given
        val plants = createDummyPlants(5)
        whenever(db.getPlants()).thenReturn(plants)

        //when
        presenter.onViewCreated()

        //then
        verify(view).showPlants(plants)
    }

    @Test
    fun on_plant_clicked_notifies_view() {
        //given
        val plant = createDummyPlants(1)
        //when
        presenter.onPlantClicked(plant[0])
        //then
        verify(view).showPlantDetailsFragment(plant[0].plantId, plant[0].mainImage)

    }

    private fun createDummyPlants(size : Int): List<Plant> {
        return List(size) { index ->
            Plant(
                index.toLong(),
                "name$index",
                "nickname$index",
                "ic_launcher_background"
//                null
            )
        }
    }

}