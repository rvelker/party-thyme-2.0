package com.example.partythyme.fragments.plant_details

import com.example.partythyme.CoroutineTestRule
import com.example.partythyme.data.PlantInterface
import com.example.partythyme.data.models.Plant
import com.example.partythyme.data.models.PlantNote
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class FragmentPlantDetailsPresenterTests {

    @get:Rule
    var ctr = CoroutineTestRule()

    private lateinit var presenter : PlantDetailsPresenter
    @Mock lateinit var view : PlantDetailsContract.ViewInterface
    @Mock lateinit var db : PlantInterface

    @Before
    fun initMocks(){
        MockitoAnnotations.initMocks(this)
        presenter = PlantDetailsPresenter(db, ctr.testDispatcherProvider)
        presenter.setView(view)
    }

    @Test
    fun on_view_created_updates_notes_on_view() = ctr.testDispatcher.runBlockingTest {
        //given

        val plant = createDummyPlant()
        val notes = createDummyNote_s(3)

        whenever(db.getNotesByPlantId(plant.plantId)).thenReturn(notes)

        //when
        presenter.onViewCreated(plant.plantId)


        //then
        verify(view).showNotes(notes)
    }

    @Test
    fun on_note_clicked_notifies_view_to_show_note_fragment(){
        //given
        val note = createDummyNote_s(1)[0]

        //when
        presenter.onNoteClicked(note)

        //then
        verify(view).showNoteFragment(note)
    }

    @Test
    fun on_add_note_clicked_shows_popup(){
        //when
        presenter.onAddNoteClicked()

        //then
        verify(view).showAddNotePopup()
    }

    @Test
    fun on_add_note_with_long_title_shows_long_error_on_view(){
        //given
        val title = "abcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaik"
        val id = 0L

        //when
        presenter.onAddNoteConfirmClicked(id,title)

        //then
        verify(view).showNewNoteTitleIsTooLongMsg()
    }

    @Test
    fun on_add_note_with_long_title_is_not_added_to_db() = ctr.testDispatcher.runBlockingTest {
        //given
        val title = "abcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaikabcgdalskfngsdaik"
        val id = 0L

        //when
        presenter.onAddNoteConfirmClicked(id, title)

        //then
        verify(db, never()).addNewNote(any())
    }

    @Test
    fun on_add_note_with_empty_title_is_not_added_to_db() = ctr.testDispatcher.runBlockingTest {
        //given
        val title = ""
        val id = 0L

        //when
        presenter.onAddNoteConfirmClicked(id, title)

        //then
        verify(db, never()).addNewNote(any())
    }

    @Test
    fun on_add_note_with_empty_title_shows_empty_error_on_view(){
        //given
        val title = ""
        val id = 0L

        //when
        presenter.onAddNoteConfirmClicked(id, title)

        //then
        verify(view).showNewNoteTitleIsEmptyMsg()
    }


    @Test
    fun on_add_note_confirm_clicked_adds_note_to_db() = ctr.testDispatcher.runBlockingTest {
        //given
        val title = "an ok number of letters for a title"
        val id = 0L

        //when
        presenter.onAddNoteConfirmClicked(id, title)

        //then
        argumentCaptor<PlantNote>().apply {
            verify(db).addNewNote(capture())
            Assert.assertEquals(title, firstValue.title)
            Assert.assertEquals(id, firstValue.noteId)
        }

    }

    @Test
    fun on_note_added_to_db_notifies_view_to_show_it() = ctr.testDispatcher.runBlockingTest {
        //given
//        val constantDate = dateUtils.getConstantDate()
        val note = createDummyNote_s(1)[0]
        whenever(db.getNoteByDate(note.date)).thenReturn(note)
        //when
        presenter.onAddNoteConfirmClicked(note.parentPlantId, note.title)

        //then
        verify(view).notifyAdapterNewNoteCreated(note)
    }

    @Test
    fun on_add_note_confirm_clicked_with_good_title_dismisses_popup(){
        //given
        val id = 0L
        val title = "an ok number of letters for a title"

        //when
        presenter.onAddNoteConfirmClicked(id, title)

        //then
        verify(view).dismissPopup()
    }
}

    private fun createDummyPlant(): Plant {
        return Plant(
                0L,
                "name",
                "nickname",
                "ic_launcher_background"
        )
    }



    private fun createDummyNote_s(listSize : Int) : List<PlantNote> {
        val date = 15428715123

        return List(listSize) {index ->
            PlantNote(index.toLong(), index.toLong(), "title", date,mutableListOf("comments"), mutableListOf("ic_launcher_background"))
        }

    }

